﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEditor;

public class LineTracer : MonoBehaviour
{
    List<Vector3> History = new List<Vector3>();
    public bool Lines;
    public bool Dots;

    // Use this for initialization
    void Start()
    {
        History.Add(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            History.Clear();
            History.Add(transform.position);
        }
        History.Add(transform.position);
    }

    public void Trace(Vector3 pos)
    {
        History.Add(pos);
    }

    void OnDrawGizmos()
    {
        try
        {
            var last = History.First();
            foreach (var pos in History)
            {
                Gizmos.color = Color.white;
                if(Lines) Gizmos.DrawLine(last, pos);
                if(Dots) Gizmos.DrawWireSphere(pos, 0.001f);
                last = pos;
            }
        }
        catch (Exception)
        {

        }
    }
}
