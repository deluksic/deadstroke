﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupTriangle : MonoBehaviour
{
    public GameObject BallPrefab;
    public float Correction;
    public float Error;
    public int Rows;
    public bool Active;

    // Use this for initialization
    void Awake()
    {
        float B_R = BallPrefab.transform.localScale.x / 2;
        if (!Active) return;
        Vector3 l = Quaternion.AngleAxis(30, Vector3.up) * transform.forward * B_R * (2 + Correction);
        Vector3 r = Quaternion.AngleAxis(-30, Vector3.up) * transform.forward * B_R * (2 + Correction);
        var nums = new Stack<int>(new int[] { 15, 14, 13, 12, 11, 10, 9, 7, 8, 6, 5, 4, 3, 2, 1 });
        for (int j = 0; j < Rows; j++)
            for (int i = 0; i < Rows - j; i++)
            {
                var obj = Instantiate(BallPrefab, transform.position + l * i + r * j + Vector3.up * B_R + Error * B_R * Vector3.Scale(Random.onUnitSphere, new Vector3(1, 0, 1)), Quaternion.identity);
                obj.GetComponent<BallBehaviour>().id = nums.Pop();
            }
    }
}
