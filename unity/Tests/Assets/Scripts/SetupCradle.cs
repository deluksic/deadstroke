﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupCradle : MonoBehaviour
{
    public GameObject BallPrefab;
    public float Correction;
    public int N;
    public bool Active;

    // Use this for initialization
    void Awake()
    {
        float B_R = BallPrefab.transform.localScale.x / 2;
        if (!Active) return;
        for (int i = 0; i < N; i++)
        {
            Instantiate(BallPrefab, ((2 + Correction) * i * transform.forward + Vector3.up) * B_R + transform.position, Quaternion.identity);
        }
    }
}
