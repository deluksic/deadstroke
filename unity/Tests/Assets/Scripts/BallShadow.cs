﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallShadow : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        var parent = transform.parent;
        var height = Mathf.Max(0, parent.position.y - parent.localScale.y * 0.5f);
        var y = 20 * height + 1f;
        var scale = 1.5f * y;
        var fade = y * y;
        transform.localScale = new Vector3(scale, scale, scale);
        transform.transform.forward = Vector3.up;
        transform.position = new Vector3(parent.position.x, 0.0005f, parent.position.z);
        GetComponent<Renderer>().material.SetColor("_TintColor", new Color(0, 0, 0, 0.5f / fade));
    }
}
