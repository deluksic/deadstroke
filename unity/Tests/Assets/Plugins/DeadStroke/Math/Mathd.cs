using System;

namespace DeadStroke.DoubleMath
{
    public struct Mathd
    {
        public const double PI = 3.14159265358979;
        public const double cbrt2 = 1.259921049894873;
        public const double Infinity = double.PositiveInfinity;
        public const double NegativeInfinity = double.NegativeInfinity;
        public const double Deg2Rad = PI / 180.0;
        public const double Rad2Deg = 180.0 / PI;
        public const double Epsilon = 1e-8;

        public static int CeilToInt(double d)
        {
            return (int)System.Math.Ceiling(d);
        }

        public static int FloorToInt(double d)
        {
            return (int)System.Math.Floor(d);
        }

        public static int RoundToInt(double d)
        {
            return (int)System.Math.Round(d);
        }

        public static double Clamp(double value, double min, double max)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }

        public static int Clamp(int value, int min, int max)
        {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        }

        public static double Clamp01(double value)
        {
            if (value < 0.0)
                return 0.0d;
            if (value > 1.0)
                return 1d;
            else
                return value;
        }

        public static double Cbrt(double value)
        {
            return Math.Pow(Math.Abs(value), 1.0 / 3) * Math.Sign(value);
        }

        public static double Lerp(double from, double to, double t)
        {
            return from + (to - from) * Mathd.Clamp01(t);
        }

        public static double LerpAngle(double a, double b, double t)
        {
            double num = Mathd.Repeat(b - a, 360d);
            if (num > 180.0d)
                num -= 360d;
            return a + num * Mathd.Clamp01(t);
        }

        public static double MoveTowards(double current, double target, double maxDelta)
        {
            if (System.Math.Abs(target - current) <= maxDelta)
                return target;
            else
                return current + System.Math.Sign(target - current) * maxDelta;
        }

        public static double MoveTowardsAngle(double current, double target, double maxDelta)
        {
            target = current + Mathd.DeltaAngle(current, target);
            return Mathd.MoveTowards(current, target, maxDelta);
        }

        public static double SmoothStep(double from, double to, double t)
        {
            t = Mathd.Clamp01(t);
            t = (-2.0 * t * t * t + 3.0 * t * t);
            return to * t + from * (1.0 - t);
        }

        public static double Gamma(double value, double absmax, double gamma)
        {
            bool flag = false;
            if (value < 0.0)
                flag = true;
            double num1 = System.Math.Abs(value);
            if (num1 > absmax)
            {
                if (flag)
                    return -num1;
                else
                    return num1;
            }
            else
            {
                double num2 = System.Math.Pow(num1 / absmax, gamma) * absmax;
                if (flag)
                    return -num2;
                else
                    return num2;
            }
        }

        public static double SmoothDamp(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed, double deltaTime)
        {
            smoothTime = System.Math.Max(0.0001d, smoothTime);
            double num1 = 2d / smoothTime;
            double num2 = num1 * deltaTime;
            double num3 = (1.0d / (1.0d + num2 + 0.479999989271164d * num2 * num2 + 0.234999999403954d * num2 * num2 * num2));
            double num4 = current - target;
            double num5 = target;
            double max = maxSpeed * smoothTime;
            double num6 = Mathd.Clamp(num4, -max, max);
            target = current - num6;
            double num7 = (currentVelocity + num1 * num6) * deltaTime;
            currentVelocity = (currentVelocity - num1 * num7) * num3;
            double num8 = target + (num6 + num7) * num3;
            if (num5 - current > 0.0 == num8 > num5)
            {
                num8 = num5;
                currentVelocity = (num8 - num5) / deltaTime;
            }
            return num8;
        }

        public static double SmoothDampAngle(double current, double target, ref double currentVelocity, double smoothTime, double maxSpeed, double deltaTime)
        {
            target = current + Mathd.DeltaAngle(current, target);
            return Mathd.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
        }

        public static double Repeat(double t, double length)
        {
            return t - System.Math.Floor(t / length) * length;
        }

        public static double PingPong(double t, double length)
        {
            t = Mathd.Repeat(t, length * 2d);
            return length - System.Math.Abs(t - length);
        }

        public static double InverseLerp(double from, double to, double value)
        {
            if (from < to)
            {
                if (value < from)
                    return 0d;
                if (value > to)
                    return 1d;
                value -= from;
                value /= to - from;
                return value;
            }
            else
            {
                if (from <= to)
                    return 0d;
                if (value < to)
                    return 1d;
                if (value > from)
                    return 0d;
                else
                    return (1.0d - (value - to) / (from - to));
            }
        }

        public static double DeltaAngle(double current, double target)
        {
            double num = Mathd.Repeat(target - current, 360d);
            if (num > 180.0d)
                num -= 360d;
            return num;
        }

        internal static bool LineIntersection(Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4, ref Vector2d result)
        {
            double num1 = p2.x - p1.x;
            double num2 = p2.y - p1.y;
            double num3 = p4.x - p3.x;
            double num4 = p4.y - p3.y;
            double num5 = num1 * num4 - num2 * num3;
            if (num5 == 0.0d)
                return false;
            double num6 = p3.x - p1.x;
            double num7 = p3.y - p1.y;
            double num8 = (num6 * num4 - num7 * num3) / num5;
            result = new Vector2d(p1.x + num8 * num1, p1.y + num8 * num2);
            return true;
        }

        internal static bool LineSegmentIntersection(Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4, ref Vector2d result)
        {
            double num1 = p2.x - p1.x;
            double num2 = p2.y - p1.y;
            double num3 = p4.x - p3.x;
            double num4 = p4.y - p3.y;
            double num5 = (num1 * num4 - num2 * num3);
            if (num5 == 0.0d)
                return false;
            double num6 = p3.x - p1.x;
            double num7 = p3.y - p1.y;
            double num8 = (num6 * num4 - num7 * num3) / num5;
            if (num8 < 0.0d || num8 > 1.0d)
                return false;
            double num9 = (num6 * num2 - num7 * num1) / num5;
            if (num9 < 0.0d || num9 > 1.0d)
                return false;
            result = new Vector2d(p1.x + num8 * num1, p1.y + num8 * num2);
            return true;
        }
    }

    public struct Quartic
    {
        private const int convSteps = 8;
        private double a, b, c, d, e;

        public Quartic(double a, double b, double c, double d, double e)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }

        public double Eval(double x)
        {
            return (((a * x + b) * x + c) * x + d) * x + e;
        }

        public double DEval(double x)
        {
            return ((4 * a * x + 3 * b) * x + 2 * c) * x + d;
        }

        public double DDEval(double x)
        {
            return (12 * a * x + 6 * b) * x + 2 * c;
        }

        /// <summary>
        /// Finds the first root after x=0 that dips below zero.
        /// In case the quartic is already below zero at zero, or
        /// has no roots until 'to', it returns NaN.
        /// </summary>
        public static double FirstRoot(double to, double step, double a, double b, double c, double d, double e)
        {
            Quartic q = new Quartic(a, b, c, d, e);
            double rb = 0;
            double lb = 0;
            double rh = 0;
            double lh = q.Eval(lb);
            // the balls are already colliding and separating
            if (lh < 0 && q.DEval(0) >= 0) return double.NaN;
            // if they are really close at start and are getting closer, return
            // epsilon as root because we want that to be a collision
            if (lh < Mathd.Epsilon && q.DEval(0) < 0)
            {
                UnityEngine.Debug.Log("Immediate root found!");
                return Math.Max(0, lh);
            }
            do
            {
                lb = rb; lh = rh;
                rb += step;
                rh = q.Eval(rb);
            } while (rh > 0 && rb <= to);
            if (rb > to) return double.NaN;
            // lb, lh, rb and rh are set to the first step which dips below 0 or after 'to'
            double p = 0.5 * (lb + rb);
            double h = q.Eval(p);
            double dh = q.DEval(p);
            for (int i = 0; i < convSteps; i++)
            {
                p -= h / dh;
                h = q.Eval(p);
                dh = q.DEval(p);
            }
            return p;
        }
    }
}
