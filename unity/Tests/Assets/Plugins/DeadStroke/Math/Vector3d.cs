using System;

namespace DeadStroke.DoubleMath
{
    public struct Vector3d
    {
        public const float kEpsilon = 1E-12f;
        public double x;
        public double y;
        public double z;

        public double this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    case 2:
                        return z;
                    default:
                        throw new IndexOutOfRangeException("Invalid index!");
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        break;
                    case 1:
                        y = value;
                        break;
                    case 2:
                        z = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid Vector3d index!");
                }
            }
        }

        public Vector3d Normalized
        {
            get
            {
                return Vector3d.Normalize(this);
            }
        }

        public double Magnitude
        {
            get
            {
                return Math.Sqrt(x * x + y * y + z * z);
            }
        }

        public double SqrMagnitude
        {
            get
            {
                return x * x + y * y + z * z;
            }
        }

        public static Vector3d Zero
        {
            get
            {
                return new Vector3d(0d, 0d, 0d);
            }
        }

        public static Vector3d One
        {
            get
            {
                return new Vector3d(1d, 1d, 1d);
            }
        }

        public static Vector3d Forward
        {
            get
            {
                return new Vector3d(0d, 0d, 1d);
            }
        }

        public static Vector3d Back
        {
            get
            {
                return new Vector3d(0d, 0d, -1d);
            }
        }

        public static Vector3d Up
        {
            get
            {
                return new Vector3d(0d, 1d, 0d);
            }
        }

        public static Vector3d Down
        {
            get
            {
                return new Vector3d(0d, -1d, 0d);
            }
        }

        public static Vector3d Left
        {
            get
            {
                return new Vector3d(-1d, 0d, 0d);
            }
        }

        public static Vector3d Right
        {
            get
            {
                return new Vector3d(1d, 0d, 0d);
            }
        }

        public static Vector3d XY
        {
            get
            {
                return new Vector3d(1d, 1d, 0d);
            }
        }

        public static Vector3d XZ
        {
            get
            {
                return new Vector3d(1d, 0d, 1d);
            }
        }

        public static Vector3d YZ
        {
            get
            {
                return new Vector3d(0d, 1d, 1d);
            }
        }

        public Vector3d(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3d(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3d(double x, double y)
        {
            this.x = x;
            this.y = y;
            z = 0d;
        }

        public static Vector3d operator +(Vector3d a, Vector3d b)
        {
            return new Vector3d(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Vector3d operator -(Vector3d a, Vector3d b)
        {
            return new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static Vector3d operator -(Vector3d a)
        {
            return new Vector3d(-a.x, -a.y, -a.z);
        }

        public static Vector3d operator *(Vector3d a, double d)
        {
            return new Vector3d(a.x * d, a.y * d, a.z * d);
        }

        public static Vector3d operator *(double d, Vector3d a)
        {
            return new Vector3d(a.x * d, a.y * d, a.z * d);
        }

        public static Vector3d operator *(Vector3d lhs, Vector3d rhs)
        {
            return new Vector3d(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
        }

        public static Vector3d operator /(Vector3d a, double d)
        {
            return new Vector3d(a.x / d, a.y / d, a.z / d);
        }

        public static bool operator ==(Vector3d lhs, Vector3d rhs)
        {
            return (lhs - rhs).SqrMagnitude < 0.0 / 1.0;
        }

        public static bool operator !=(Vector3d lhs, Vector3d rhs)
        {
            return (lhs - rhs).SqrMagnitude >= 0.0 / 1.0;
        }

        public static Vector3d Lerp(Vector3d from, Vector3d to, double t)
        {
            t = Mathd.Clamp01(t);
            return new Vector3d(from.x + (to.x - from.x) * t, from.y + (to.y - from.y) * t, from.z + (to.z - from.z) * t);
        }

        public static Vector3d MoveTowards(Vector3d current, Vector3d target, double maxDistanceDelta)
        {
            Vector3d vector3 = target - current;
            double magnitude = vector3.Magnitude;
            if (magnitude <= maxDistanceDelta || magnitude == 0.0d)
                return target;
            else
                return current + vector3 / magnitude * maxDistanceDelta;
        }

        public static Vector3d SmoothDamp(Vector3d current, Vector3d target, ref Vector3d currentVelocity, double smoothTime, double maxSpeed, double deltaTime)
        {
            smoothTime = Math.Max(0.0001d, smoothTime);
            double num1 = 2d / smoothTime;
            double num2 = num1 * deltaTime;
            double num3 = (1.0d / (1.0d + num2 + 0.479999989271164d * num2 * num2 + 0.234999999403954d * num2 * num2 * num2));
            Vector3d vector = current - target;
            Vector3d vector3_1 = target;
            double maxLength = maxSpeed * smoothTime;
            Vector3d vector3_2 = Vector3d.ClampMagnitude(vector, maxLength);
            target = current - vector3_2;
            Vector3d vector3_3 = (currentVelocity + num1 * vector3_2) * deltaTime;
            currentVelocity = (currentVelocity - num1 * vector3_3) * num3;
            Vector3d vector3_4 = target + (vector3_2 + vector3_3) * num3;
            if (Vector3d.Dot(vector3_1 - current, vector3_4 - vector3_1) > 0.0)
            {
                vector3_4 = vector3_1;
                currentVelocity = (vector3_4 - vector3_1) / deltaTime;
            }
            return vector3_4;
        }

        public void Set(double new_x, double new_y, double new_z)
        {
            x = new_x;
            y = new_y;
            z = new_z;
        }

        public static Vector3d Scale(Vector3d a, Vector3d b)
        {
            return new Vector3d(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public void Scale(Vector3d scale)
        {
            x *= scale.x;
            y *= scale.y;
            z *= scale.z;
        }

        public static Vector3d Cross(Vector3d lhs, Vector3d rhs)
        {
            return new Vector3d(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() << 2 ^ z.GetHashCode() >> 2;
        }

        public override bool Equals(object other)
        {
            if (!(other is Vector3d))
                return false;
            Vector3d vector3d = (Vector3d)other;
            if (x.Equals(vector3d.x) && y.Equals(vector3d.y))
                return z.Equals(vector3d.z);
            else
                return false;
        }

        public static Vector3d Reflect(Vector3d inDirection, Vector3d inNormal)
        {
            return -2d * Vector3d.Dot(inNormal, inDirection) * inNormal + inDirection;
        }

        public static Vector3d Normalize(Vector3d value)
        {
            double num = value.Magnitude;
            if (num > 9.99999974737875E-06)
                return value / num;
            else
                return Vector3d.Zero;
        }

        public void Normalize()
        {
            double num = Magnitude;
            if (num > 9.99999974737875E-06)
                this = this / num;
            else
                this = Vector3d.Zero;
        }
        // TODO : fix formatting
        public override string ToString()
        {
            return "(" + x + " - " + y + " - " + z + ")";
        }

        public static double Dot(Vector3d lhs, Vector3d rhs)
        {
            return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
        }

        public static Vector3d Project(Vector3d vector, Vector3d onNormal)
        {
            double num = Vector3d.Dot(onNormal, onNormal);
            if (num < 1.40129846432482E-45d)
                return Vector3d.Zero;
            else
                return onNormal * Vector3d.Dot(vector, onNormal) / num;
        }

        public static Vector3d Exclude(Vector3d excludeThis, Vector3d fromThat)
        {
            return fromThat - Vector3d.Project(fromThat, excludeThis);
        }

        public static double Distance(Vector3d a, Vector3d b)
        {
            Vector3d vector3d = new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
            return Math.Sqrt(vector3d.x * vector3d.x + vector3d.y * vector3d.y + vector3d.z * vector3d.z);
        }

        public static Vector3d ClampMagnitude(Vector3d vector, double maxLength)
        {
            if (vector.SqrMagnitude > maxLength * maxLength)
                return vector.Normalized * maxLength;
            else
                return vector;
        }

        public static Vector3d Min(Vector3d lhs, Vector3d rhs)
        {
            return new Vector3d(Math.Min(lhs.x, rhs.x), Math.Min(lhs.y, rhs.y), Math.Min(lhs.z, rhs.z));
        }

        public static Vector3d Max(Vector3d lhs, Vector3d rhs)
        {
            return new Vector3d(Math.Max(lhs.x, rhs.x), Math.Max(lhs.y, rhs.y), Math.Max(lhs.z, rhs.z));
        }
    }
}
