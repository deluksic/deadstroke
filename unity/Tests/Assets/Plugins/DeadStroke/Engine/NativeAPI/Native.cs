﻿/* 
 * Native dll invocation helper by Francis R. Griffiths-Keam
 * www.runningdimensions.com/blog
 */

using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace DeadStroke.Engine.NativeAPI
{
    public static class Native
    {
        public static T Invoke<T, T2>(IntPtr library, params object[] pars)
        {
            string name = typeof(T2).Name.TrimStart('_');
            IntPtr funcPtr = GetProcAddress(library, name);
            if (funcPtr == IntPtr.Zero)
            {
                Debug.LogWarning("Could not gain reference to method address. " + name);
                return default(T);
            }

            var func = Marshal.GetDelegateForFunctionPointer(funcPtr, typeof(T2));
            return (T)func.DynamicInvoke(pars);
        }

        public static void Invoke<T>(IntPtr library, params object[] pars)
        {
            string name = typeof(T).Name.TrimStart('_');
            IntPtr funcPtr = GetProcAddress(library, name);
            if (funcPtr == IntPtr.Zero)
            {
                Debug.LogWarning("Could not gain reference to method address. " + name);
                return;
            }

            var func = Marshal.GetDelegateForFunctionPointer(funcPtr, typeof(T));
            func.DynamicInvoke(pars);
        }

        [DllImport("kernel32", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32")]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);
    }
}