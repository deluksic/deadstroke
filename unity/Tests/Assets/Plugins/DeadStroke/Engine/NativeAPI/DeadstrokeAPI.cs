﻿using System;
using System.Runtime.InteropServices;
using DeadStroke.DoubleMath;
using DeadStroke.Engine.Colliders;
using UnityEngine;

namespace DeadStroke.Engine.NativeAPI
{
    public static class DeadStrokeAPI
    {
        #region Static dll interop

        static IntPtr _lptr = IntPtr.Zero;

#if UNITY_STANDALONE_WIN
        const string _dllpath = "DeadStrokeAPI";
#else
        const string _dllpath = "";
#endif


        delegate IntPtr _CreateSimulation([In] BallInfo[] ballinfo, [In, Out] BallState[] balls, int b_count, SimParams ps);
        delegate void _DeleteSimulation(IntPtr sim);
        delegate int _StepSimulation(IntPtr sim, double dt);
        delegate double _GetTimeSimulation(IntPtr sim);
        delegate void _RegisterPlaneCollider(IntPtr sim, Vector3d p, Vector3d n, double b, double f, double s);
        delegate void _RegisterCapsuleCollider(IntPtr sim, Vector3d p1, Vector3d p2, double r, double b, double f, double s);
        delegate void _RegisterBoundedPlaneCollider(IntPtr sim, Vector3d p, Vector3d u, Vector3d v, double b, double f, double s);
        delegate void _RegisterRod(IntPtr sim, int bid, double d, Vector3d p);
        delegate void _InitSimulation(IntPtr sim);

        #endregion

        /// <summary>
        /// This function should be called first to load the library before any other call.
        /// Be sure to UnloadLibrary() once you're done with it.
        /// </summary>
        /// <returns>True if the library was loaded.</returns>
        public static bool LoadLibrary()
        {
            if (_lptr != IntPtr.Zero) return false; // library already loaded.

            
            _lptr = Native.LoadLibrary(_dllpath);
            if (_lptr == IntPtr.Zero)
            {
                Debug.LogError("Failed to load native library");
                return false;
            }
            Debug.Log("Loaded " + _dllpath);
            return true;
        }

        /// <summary>
        /// This is the function to be called after everything is finished. 
        /// Usually in the OnApplicationQuit event.
        /// </summary>
        /// <returns>True if the library unloaded correctly.</returns>
        public static bool UnloadLibrary()
        {
            if (_lptr == IntPtr.Zero) return false; // no library loaded.

            if (Native.FreeLibrary(_lptr))
            {
                Debug.Log("Native library successfully unloaded.");
                return true;
            }
            Debug.LogError("Native library could not be unloaded.");
            return false;
        }

        public static IntPtr CreateSimulation(BallInfo[] ballinfo, BallState[] balls, SimParams ps)
        {
            return Native.Invoke<IntPtr, _CreateSimulation>(_lptr, ballinfo, balls, balls.Length, ps);
        }

        public static void DeleteSimulation(IntPtr sim)
        {
            Native.Invoke<_DeleteSimulation>(_lptr, sim);
        }

        public static int StepSimulation(IntPtr sim, double dt)
        {
            return Native.Invoke<int, _StepSimulation>(_lptr, sim, dt);
        }

        public static void InitSimulation(IntPtr sim)
        {
            Native.Invoke<_InitSimulation>(_lptr, sim);
        }

        public static double GetTimeSimulation(IntPtr sim)
        {
            return Native.Invoke<double, _GetTimeSimulation>(_lptr, sim);
        }

        public static void RegisterPlaneCollider(IntPtr sim, PlaneCollider c)
        {
            Native.Invoke<_RegisterPlaneCollider>(_lptr, sim, c.p, c.n, c.C_Rest, c.C_Fric, c.C_Stif);
        }

        public static void RegisterBoundedPlaneCollider(IntPtr sim, BoundedPlaneCollider c)
        {
            Native.Invoke<_RegisterBoundedPlaneCollider>(_lptr, sim, c.p, c.u, c.v, c.C_Rest, c.C_Fric, c.C_Stif);
        }

        public static void RegisterCapsuleCollider(IntPtr sim, Colliders.CapsuleCollider c)
        {
            Native.Invoke<_RegisterCapsuleCollider>(_lptr, sim, c.p1, c.p2, c.r, c.C_Rest, c.C_Fric, c.C_Stif);
        }

        public static void RegisterRod(IntPtr sim, Rod r)
        {
            Native.Invoke<_RegisterRod>(_lptr, sim, r.bid, r.d, r.p);
        }
    }
}
