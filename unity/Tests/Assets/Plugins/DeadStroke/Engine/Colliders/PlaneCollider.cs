﻿using DeadStroke.DoubleMath;
using DeadStroke.Engine.NativeAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeadStroke.Engine.Colliders
{
    public class PlaneCollider : StaticCollider
    {
        public Vector3d p, n;

        public PlaneCollider(Vector3d p, Vector3d n)
        {
            this.p = p;
            this.n = n;
        }

        public override void Register(IntPtr simulation)
        {
            DeadStrokeAPI.RegisterPlaneCollider(simulation, this);
        }
    }
}
