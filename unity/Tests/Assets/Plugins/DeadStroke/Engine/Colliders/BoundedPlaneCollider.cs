﻿using DeadStroke.DoubleMath;
using DeadStroke.Engine.NativeAPI;
using System;

namespace DeadStroke.Engine.Colliders
{
    public class BoundedPlaneCollider : StaticCollider
    {
        public Vector3d p, u, v;

        public override void Register(IntPtr simulation)
        {
            DeadStrokeAPI.RegisterBoundedPlaneCollider(simulation, this);
        }
    }
}
