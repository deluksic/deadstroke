﻿using DeadStroke.DoubleMath;
using DeadStroke.Engine.NativeAPI;
using System;

namespace DeadStroke.Engine.Colliders
{
    public class CapsuleCollider : StaticCollider
    {
        public Vector3d p1, p2;
        public double r;

        public CapsuleCollider(Vector3d p1, Vector3d p2, double r)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.r = r;
        }

        public override void Register(IntPtr simulation)
        {
            DeadStrokeAPI.RegisterCapsuleCollider(simulation, this);
        }
    }
}
