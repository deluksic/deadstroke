﻿namespace DeadStroke.Engine.Colliders
{
    public abstract class StaticCollider : Actor
    {
        public double C_Rest = 4e1;
        public double C_Fric = 0.2;
        public double C_Stif = 1e7;
    }
}
