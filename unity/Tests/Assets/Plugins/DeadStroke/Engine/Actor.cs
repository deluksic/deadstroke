﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeadStroke.Engine
{
    public abstract class Actor
    {
        public abstract void Register(IntPtr simulation);
    }
}
