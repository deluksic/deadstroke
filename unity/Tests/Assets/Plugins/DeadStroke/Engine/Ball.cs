﻿using DeadStroke.DoubleMath;
using System;
using System.Runtime.InteropServices;

namespace DeadStroke.Engine
{
    public struct BallState
    {
        public Vector3d p, v, w;
    }

    public struct BallInfo
    {
        public double m;
        public double r;
    }

    public class Ball
    {
        public int sim_id; // sim_id is used for referencing
        public int id; // id is used only for gameplay purposes
        public Vector3d p, v, w;
        public double m;
        public double r;

        public Ball(int id, double m, double r, Vector3d p, Vector3d v, Vector3d w)
        {
            this.sim_id = -1; // still not defined, will be when added to simulation
            this.id = id;
            this.m = m;
            this.r = r;
            this.p = p;
            this.v = v;
            this.w = w;
        }
    }
}
