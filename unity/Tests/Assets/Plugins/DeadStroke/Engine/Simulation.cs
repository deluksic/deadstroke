﻿using DeadStroke.Engine.Colliders;
using DeadStroke.Engine.NativeAPI;
using System;
using System.Collections.Generic;

namespace DeadStroke.Engine
{
    public struct SimParams
    {
        public double G;
        public double C_TableRest;
        public double C_TableStiff;
        public double C_TableSlidFric;
        public double C_TableRollFric;
        public double C_TableSpinFric;
        public double C_BallFric;
        public double C_BallStiff;
        public double C_BallRest;
        public double Precision;
        public double MaxStep;
    };

    public class Simulation : IDisposable
    {
        private IntPtr _ptr = IntPtr.Zero;

        private BallState[] _bStates;
        private List<Ball> registeredBalls;
        private List<Actor> registeredActors;

        private SimParams _params;

        public Simulation(SimParams ps)
        {
            _params = ps;
            registeredBalls = new List<Ball>();
            registeredActors = new List<Actor>();
        }

        public void RegisterBall(Ball b)
        {
            b.sim_id = registeredBalls.Count;
            registeredBalls.Add(b);
        }

        public void RegisterActor(Actor c)
        {
            registeredActors.Add(c);
        }

        public void Initialize()
        {
            if (_ptr != IntPtr.Zero)
                throw new Exception("Already initialized!");

            _bStates = new BallState[registeredBalls.Count];
            var ballinfo = new BallInfo[registeredBalls.Count];
            for (int i = 0; i < _bStates.Length; i++)
            {
                _bStates[i].p = registeredBalls[i].p;
                _bStates[i].v = registeredBalls[i].v;
                _bStates[i].w = registeredBalls[i].w;
                ballinfo[i].m = registeredBalls[i].m;
                ballinfo[i].r = registeredBalls[i].r;
            }
            _ptr = DeadStrokeAPI.CreateSimulation(ballinfo, _bStates, _params);

            foreach (var c in registeredActors)
                c.Register(_ptr);

            DeadStrokeAPI.InitSimulation(_ptr);
        }

        public int Step(double dt)
        {
            int steps = DeadStrokeAPI.StepSimulation(_ptr, dt);
            for (int i = 0; i < _bStates.Length; i++)
            {
                registeredBalls[i].p = _bStates[i].p;
                registeredBalls[i].v = _bStates[i].v;
                registeredBalls[i].w = _bStates[i].w;
            }
            return steps;
        }

        public void Dispose()
        {
            if (_ptr != IntPtr.Zero)
                DeadStrokeAPI.DeleteSimulation(_ptr);
        }
    }
}
