﻿using System;
using DeadStroke.Engine.NativeAPI;
using DeadStroke.DoubleMath;

namespace DeadStroke.Engine
{
    public class Rod : Actor
    {
        public int bid;
        public Vector3d p;
        public double d;

        public Rod(int bid, double d, Vector3d p)
        {
            this.bid = bid;
            this.d = d;
            this.p = p;
        }

        public override void Register(IntPtr simulation)
        {
            DeadStrokeAPI.RegisterRod(simulation, this);
        }
    }
}
