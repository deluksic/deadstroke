using DeadStroke.DoubleMath;

namespace DeadStroke.Unity.Convertors
{
    public static class Vector2dConvertor
    {
        public static UnityEngine.Vector2 ToUnity(this Vector2d v)
        {
            return new UnityEngine.Vector2((float)v.x, (float)v.y);
        }

        public static Vector2d AsDouble(this UnityEngine.Vector2 v)
        {
            return new Vector2d(v.x, v.y);
        }
    }

    public static class Vector3dConvertor
    {
        public static UnityEngine.Vector3 ToUnity(this Vector3d v)
        {
            return new UnityEngine.Vector3((float)v.x, (float)v.y, (float)v.z);
        }

        public static Vector3d AsDouble(this UnityEngine.Vector3 v)
        {
            return new Vector3d(v.x, v.y, v.z);
        }
    }

    public static class QuaternionConvertor
    {
        public static UnityEngine.Quaternion ToUnity(this Quaternion v)
        {
            return new UnityEngine.Quaternion((float)v.x, (float)v.y, (float)v.z, (float)v.w);
        }

        public static Quaternion AsDouble(this UnityEngine.Quaternion v)
        {
            return new Quaternion(v.x, v.y, v.z, v.w);
        }
    }
}
