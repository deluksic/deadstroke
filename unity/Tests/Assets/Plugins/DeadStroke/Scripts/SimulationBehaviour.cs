﻿using UnityEngine;
using DeadStroke.Engine;
using DeadStroke.Engine.NativeAPI;

public class SimulationBehaviour : Singleton<SimulationBehaviour>
{
    private Simulation sim;

    [HideInInspector]
    public double dt = 1.0 / 60;

    public SimulationBehaviour()
    {
        SimParams ps;
        ps.C_BallFric = 0.08;
        ps.C_BallStiff = 1e11;
        ps.C_BallRest = 1e2;
        ps.C_TableRollFric = 0.02;
        ps.C_TableSlidFric = 0.2;
        ps.C_TableSpinFric = 1;
        ps.C_TableRest = 1.5e3;
        ps.C_TableStiff = 1e9;
        ps.G = 9.81;
        ps.MaxStep = dt;
        ps.Precision = 1e-6;
        sim = new Simulation(ps);
    }

    private void Start()
    {
        DeadStrokeAPI.LoadLibrary();

        foreach (var b in FindObjectsOfType<BallBehaviour>())
            b.Register(this);

        foreach (var a in FindObjectsOfType<ActorBehaviour>())
            a.Register(this);

        sim.Initialize();
    }
    
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.A))
            dt = 1e-5;
        if (Input.GetKeyDown(KeyCode.S))
            dt = 1e-4;
        if (Input.GetKeyDown(KeyCode.D))
            dt = 2e-3;
        if (Input.GetKeyDown(KeyCode.F))
            dt = 1.0 / 60;

        if (Input.GetKey(KeyCode.Space))
        {
            int steps = sim.Step(dt);
            if (steps > 30)
                Debug.Log(steps);
        }
    }

    private void OnApplicationQuit()
    {
        sim.Dispose();
        DeadStrokeAPI.UnloadLibrary();
    }

    public void RegisterBall(Ball b)
    {
        sim.RegisterBall(b);
    }

    public void RegisterActor(Actor c)
    {
        sim.RegisterActor(c);
    }
}
