﻿using UnityEngine;
using System.Collections;

public abstract class SimulationObjectBehaviour : MonoBehaviour
{
    public abstract void Register(SimulationBehaviour sim);
}
