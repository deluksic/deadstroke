﻿using DeadStroke.Engine;
using DeadStroke.Unity.Convertors;
using UnityEngine;

public class BallBehaviour : SimulationObjectBehaviour
{
    public int id = -1;
    public double m;
    public Vector3 v, w;

    public Ball b;

    public override void Register(SimulationBehaviour sim)
    {
        b = new Ball(
            id == -1 ? Random.Range(1, 16) : id,
            m, transform.localScale.x * 0.5,
            transform.position.AsDouble(), 
            v.AsDouble(), 
            w.AsDouble()
        );
        sim.RegisterBall(b);
    }
    
    void Start()
    {
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        block.SetVector("_Region", new Vector4(id % 4, id / 4) * 0.25f);
        GetComponent<Renderer>().SetPropertyBlock(block);
    }
    
    void FixedUpdate()
    {
        transform.position = b.p.ToUnity();
        transform.Rotate(b.w.ToUnity(), (float)(b.w.Magnitude * SimulationBehaviour.Instance.dt) * Mathf.Rad2Deg, Space.World);
    }
}
