﻿using DeadStroke.Engine;
using DeadStroke.Engine.Colliders;
using DeadStroke.Unity.Convertors;
using UnityEngine;

public class RodBehaviour : ActorBehaviour
{
    public BallBehaviour b;
    public double d;

    private LineRenderer lr;

    public override void Register(SimulationBehaviour sim)
    {
        SimulationBehaviour.Instance.RegisterActor(
                new Rod(b == null || b.b == null ? -1 : b.b.sim_id, d, transform.position.AsDouble()));
    }
    
    private void Start()
    {
        if (b == null) return;

        lr = gameObject.AddComponent<LineRenderer>();
        lr.startWidth = lr.endWidth = 0.01f;
        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, b.transform.position);
    }

    private void Update()
    {
        if (b == null) return;

        lr.SetPosition(1, b.transform.position);
    }
}
