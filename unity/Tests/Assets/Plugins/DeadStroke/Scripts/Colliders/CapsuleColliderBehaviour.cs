﻿using DeadStroke.Engine;
using DeadStroke.Engine.Colliders;
using DeadStroke.Unity.Convertors;
using UnityEngine;

public class CapsuleColliderBehaviour : ActorBehaviour
{
    public double r;
    public Vector3 p1, p2;
    [HideInInspector]
    public UnityEngine.CapsuleCollider c;

    public override void Register(SimulationBehaviour sim)
    {
        if (c != null)
            sim.RegisterActor(
                new DeadStroke.Engine.Colliders.CapsuleCollider(
                    (transform.position + transform.up * (c.height / 2 - c.radius)).AsDouble(),
                    (transform.position - transform.up * (c.height / 2 - c.radius)).AsDouble(),
                    c.radius));
        else
            sim.RegisterActor(
                new DeadStroke.Engine.Colliders.CapsuleCollider(
                    p1.AsDouble(),
                    p2.AsDouble(),
                    r));
    }
}
