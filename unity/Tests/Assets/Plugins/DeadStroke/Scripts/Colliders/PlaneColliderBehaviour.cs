﻿using System;
using DeadStroke.Engine;
using DeadStroke.Engine.Colliders;
using DeadStroke.Unity.Convertors;
using UnityEngine;

public class PlaneColliderBehaviour : ActorBehaviour
{
    public Transform p;

    public override void Register(SimulationBehaviour sim)
    {
        sim.RegisterActor(
            new PlaneCollider(
                p.position.AsDouble(),
                p.up.AsDouble()));
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.matrix = p.localToWorldMatrix;
        Gizmos.color = new Color(0, 1, 0, 0.2f);
        Gizmos.DrawCube(Vector3.zero, new Vector3(100, 0.0001f, 100));
        Gizmos.matrix = Matrix4x4.identity;
    }
}
