﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CapsuleColliderBehaviour))]
public class CapsuleColliderEditor : Editor
{
    enum Options
    {
        Collider,
        Custom
    }

    Options op = Options.Collider;

    public override void OnInspectorGUI()
    {
        op = (Options)EditorGUILayout.EnumPopup("Choose init method:", op);
        CapsuleColliderBehaviour cb = (CapsuleColliderBehaviour)target;
        if (op == Options.Collider)
        {
            cb.c = (CapsuleCollider)EditorGUILayout.ObjectField("Collider", cb.c, typeof(CapsuleCollider), true);
        }
        else
        {
            base.OnInspectorGUI();
        }
    }
}
