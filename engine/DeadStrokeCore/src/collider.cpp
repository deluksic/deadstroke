#include "deadstroke.h"
#include "ball.h"
#include "actor.h"
#include "collider.h"
#include "simulation.h"

namespace deadstroke {

	void StaticCollider::Resolve(const Ball &b, const BallState& bs, DBallState& db) const
	{
		CollisionResult cr = Calculate(b, bs);
		if (cr.d > 0) {
			vec_type nr = cr.n * b.r;
			value_type vl = dot(bs.v, cr.n);
			value_type f = (ramp3over2(cr.d) * C_Stif - vl * C_Rest);
			vec_type ff = f * cr.n;
			db.dv += ff;
			vec_type cpdv = cross(cross(cr.n, bs.v), cr.n);
			vec_type cp = cpdv + cross(nr, bs.w);
			value_type cpl = dot(cp, cp);
			ff = (f * C_Fric * fricstep(cpl*1.0e3)) * normalize(cp);
			b.AddRelativeForce(db, -ff, -nr);
		}
	}

	CollisionResult PlaneCollider::Calculate(const Ball &b, const BallState &bs) const
	{
		CollisionResult cr;
		cr.n = n;
		cr.d = b.r - dot(bs.p - p, n);
		return cr;
	}

	CollisionResult CapsuleCollider::Calculate(const Ball &b, const BallState &bs) const
	{
		CollisionResult cr;
		vec_type d1 = bs.p - p1;
		vec_type d2 = bs.p - p2;
		vec_type rv;
		if (dot(d1, n) < 0)
			rv = d1;
		else if (dot(d2, n) > 0)
			rv = d2;
		else
			rv = cross(n, cross(d1, n));
		value_type rvl = length(rv);
		cr.d = b.r + r - rvl;
		cr.n = rv / rvl;
		return cr;
	}

	CollisionResult BoundedPlaneCollider::Calculate(const Ball &b, const BallState &bs) const
	{
		CollisionResult cr;
		vec_type d = bs.p - p;
		value_type du = dot(d, u);
		value_type dv = dot(d, v);
		if (du < 0 || dv < 0 || du > ul || dv > vl) {
			cr.d = -1;
			return cr;
		}
		cr.n = n;
		cr.d = b.r - dot(bs.p - p, n);
		return cr;
	}
}
