#pragma once

namespace deadstroke {

	class Actor
	{
	public:
		virtual void Resolve(const Ball &b, const BallState &bs, DBallState &db) const = 0;
	};

	class Rod : public Actor
	{
	public:
		Rod(int bid, value_type d, vec_type p) : bid(bid), d(d), p(p) {}
		virtual void Resolve(const Ball &b, const BallState &bs, DBallState &db) const;
	private:
		int bid;
		value_type d;
		vec_type p;
	};
}

