#include "deadstroke.h"
#include "ball.h"
#include "actor.h"

namespace deadstroke {

	void Rod::Resolve(const Ball & b, const BallState & bs, DBallState & db) const
	{
		if (bid != -1 && b.id != bid) return;
		vec_type delta = p - bs.p;
		value_type deltal = length(delta);
		value_type dd = deltal - d;
		vec_type deltan = delta / deltal;
		value_type vl = dot(bs.v, deltan);
		value_type f = (dd * 1.0e8 - vl * 1.0e3);
		vec_type ff = f * deltan;
		db.dv += ff * b.m_1;
	}

}
