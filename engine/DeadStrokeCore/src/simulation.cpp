#include "deadstroke.h"
#include "ball.h"
#include "actor.h"
#include "collider.h"
#include "simulation.h"

namespace deadstroke {

	Simulation::Simulation(BallInfo* b_info, BallState* bs, size_t b_count, SimParams params) :
		b_states(bs),
		b_count(b_count),
		stepper(params.Precision, params.Precision, 1.0, 1.0, params.MaxStep, false),
		actors(),
		balls(),
		state(),
		t(0.0),
		G(params.G),
		C_TableRest(params.C_TableRest),
		C_TableStiff(params.C_TableStiff),
		C_TableSlidFric(params.C_TableSlidFric),
		C_TableRollFric(params.C_TableRollFric),
		C_TableSpinFric(params.C_TableSpinFric),
		C_BallFric(params.C_BallFric),
		C_BallStiff(params.C_BallStiff),
		C_BallRest(params.C_BallRest),
		Precision(params.Precision),
		MaxStep(params.MaxStep)
	{
		for (int i = 0; i < b_count; i++)
			balls.push_back(Ball(i, b_info[i].m, b_info[i].r));
	}

	Simulation::~Simulation()
	{

	}

	void Simulation::operator()(const state_type &x, state_type &dxdt, const value_type t) const {
		BallState* bs = (BallState*)&x[0];
		DBallState* dbs = (DBallState*)&dxdt[0];
		for (int i = 0; i < b_count; i++) {
			if (!balls[i].active) continue;
			StepBall(bs[i], dbs[i]);
			AddTableForce(balls[i], bs[i], dbs[i]);
			for (Actor* a : actors) {
				a->Resolve(balls[i], bs[i], dbs[i]);
			}
		}
		for (int i = 0; i < balls.size(); i++) {
			for (int j = i + 1; j < balls.size(); j++) {
				if (balls[i].active || balls[i].active)
					BallBallCollision(balls[i], balls[j], bs[i], bs[j], dbs[i], dbs[j]);
			}
		}
	}

	void Simulation::RegisterActor(Actor* actor) {
		actors.push_back(actor);
	}

	inline void Simulation::StepBall(const BallState &b, DBallState &db) const {
		db.dp = b.v;
		db.dv = vec_type(0, -G, 0);
		db.dw = vec_type(0, 0, 0);
	}

	inline void Simulation::AddTableForce(const Ball &b, const BallState &bs, DBallState &db) const {
		value_type h = b.r - bs.p.y;
		if (h > 0) {
			value_type f = ramp3over2(h) * C_TableStiff - bs.v.y * C_TableRest;
			db.dv.y += f * b.m_1;
			vec_type down(0, -b.r, 0);
			vec_type vxz(-bs.v.x, 0.0, -bs.v.z);
			vec_type cp = vxz + cross(down, bs.w);
			value_type cpl = dot(cp, cp);
			value_type vxzl = dot(vxz, vxz);
			vec_type ff(0, 0, 0);
			if (cpl > 1.0e-6) {
				ff = (f * C_TableSlidFric * fricstep(cpl*1.0e6)) * normalize(cp);
				b.AddRelativeForce(db, ff, down);
			}
			else if (vxzl > 1.0e-6) {
				ff = (f * C_TableRollFric * fricstep(vxzl*1.0e6)) * normalize(vxz);
				db.dv += ff;
				db.dw -= cross(down, ff) / (b.r*b.r);
			}
			if (abs(bs.w.y) > 1.0e-8) {
				db.dw.y -= f * C_TableSpinFric * fricstep(bs.w.y*1.0e6);
			}
		}
	}

	inline void Simulation::BallBallCollision(
		const Ball &b1,
		const Ball &b2,
		const BallState &bs1,
		const BallState &bs2,
		DBallState &db1,
		DBallState &db2) const
	{
		vec_type dp = bs1.p - bs2.p;
		value_type dpl = length(dp);
		value_type d = b1.r + b2.r - dpl;
		if (d > 0) {
			vec_type dpn = dp / dpl;
			vec_type dpnr1 = dpn * b1.r;
			vec_type dpnr2 = dpn * b2.r;
			vec_type dv = bs1.v - bs2.v;
			value_type dvl = dot(dv, dpn);
			value_type f = ramp3over2(d) * C_BallStiff - dvl * C_BallRest;
			vec_type ff = f * dpn;
			db1.dv += ff * b1.m_1;
			db2.dv -= ff * b2.m_1;
			vec_type cpdv = cross(cross(dpn, dv), dpn);
			vec_type cp = cpdv + cross(dpnr1, bs1.w) + cross(dpnr2, bs2.w);
			value_type cpl = dot(cp, cp);
			if (cpl > 1e-6) {
				ff = (f * C_BallFric * fricstep(cpl*1e4)) * normalize(cp);
				b1.AddRelativeForce(db1, -ff, -dpnr1);
				b2.AddRelativeForce(db2, ff, dpnr2);
			}
		}
	}

	int Simulation::Step(value_type dt)
	{
		int steps_limit = 500, steps = 0;

		t += dt;
		while ((stepper.current_time() < t)) {
			stepper.do_step(*this);
			if (++steps >= steps_limit) {
				state = stepper.current_state();
				goto overflowed;
			}
		}
		// this will be skipped if overflowed, and sim will slow down
		stepper.calc_state(t, state);

	overflowed:

		// TODO: put sleeping balls to sleep

		// copy state back to b_states
		for (int i = 0; i < b_count; ++i) {
			for (int j = 0; j < bsoffset; ++j) {
				((value_type*)&(b_states[i]))[j] = state[i*bsoffset + j];
			}
		}
		return steps;
	}

	void Simulation::Initialize() {

		state.clear();
		// copy state from b_states and initialize stepper with it
		for (int i = 0; i < balls.size(); i++) {
			for (int j = 0; j < bsoffset; j++) {
				state.push_back(((value_type*)&(b_states[i]))[j]);
			}
		}
		stepper.initialize(state, t, MaxStep);
	}

	double Simulation::CurrentTime() {
		return t;
	}

}
