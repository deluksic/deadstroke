#pragma once
#include "type_definitions.h"

namespace deadstroke {

	struct BallState {
		vec_type p, v, w;
	};

	struct DBallState {
		vec_type dp, dv, dw;
	};

	struct BallInfo {
		value_type m;
		value_type r;
	};

	class Ball {
	public:

		int id;
		value_type m;
		value_type r;
		value_type m_1;
		value_type i_1;
		bool active;

		Ball(int id, value_type m, value_type r) :
			id(id),
			m(m),
			r(r),
			m_1(1.0 / m),
			i_1(5.0 / (2.0*m*r*r)),
			active(true) {}
		inline void AddRelativeForce(DBallState &db, vec_type f, vec_type p) const {
			db.dv += f*m_1;
			db.dw += cross(p, f)*i_1;
		}
		inline void PutToSleep(BallState &b) {
			b.v = b.w = vec_type(0);
			active = false;
		}
	};
}

