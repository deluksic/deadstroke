#pragma once

namespace deadstroke {

	struct CollisionResult {
	public:
		// normal component of the collider at collision point
		// pointing OUTSIDE the collider
		vec_type n;
		// distance of penetration along normal axis
		value_type d;
	};

	class StaticCollider : public Actor
	{
	private:
		const value_type C_Rest;
		const value_type C_Fric;
		const value_type C_Stif;
	public:
		StaticCollider(value_type c_rest, value_type c_fric, value_type c_stif) :
			C_Rest(c_rest),
			C_Fric(c_fric),
			C_Stif(c_stif)
		{}
		virtual CollisionResult Calculate(const Ball &b, const BallState &bs) const = 0;
		virtual void Resolve(const Ball &b, const BallState &bs, DBallState &db) const;
	};

	class PlaneCollider : public StaticCollider {
	protected:
		vec_type p, n;
	public:
		PlaneCollider
		(
			vec_type p,
			vec_type n,
			value_type c_rest,
			value_type c_fric,
			value_type c_stif
		) :
			StaticCollider(c_rest, c_fric, c_stif),
			p(p),
			n(n) {}
		virtual CollisionResult Calculate(const Ball &b, const BallState &bs) const;
	};

	class CapsuleCollider : public StaticCollider {
	protected:
		vec_type p1, p2, n;
		value_type r;
	public:
		CapsuleCollider
		(
			vec_type p1,
			vec_type p2,
			value_type r,
			value_type c_rest,
			value_type c_fric,
			value_type c_stif
		) :
			StaticCollider(c_rest, c_fric, c_stif),
			p1(p1),
			p2(p2),
			r(r),
			n(normalize(p2 - p1)) {}
		virtual CollisionResult Calculate(const Ball &b, const BallState &bs) const;
	};

	class BoundedPlaneCollider : public StaticCollider {
	protected:
		value_type ul, vl;
		vec_type p, u, v, n;
	public:
		BoundedPlaneCollider
		(
			vec_type p,
			vec_type u,
			vec_type v,
			value_type c_rest,
			value_type c_fric,
			value_type c_stif
		) :
			StaticCollider(c_rest, c_fric, c_stif),
			ul(length(u)),
			vl(length(v)),
			p(p),
			u(normalize(u)),
			v(normalize(v)),
			n(normalize(cross(u, v))) {}
		virtual CollisionResult Calculate(const Ball &b, const BallState &bs) const;
	};
}

