#pragma once

#include <vector>

#include "type_definitions.h"
#include "ball.h"
#include "collider.h"
#include "actor.h"

namespace deadstroke {

	inline value_type fricstep(value_type fric) {
		fric = clamp(fric, value_type(-1), value_type(1));
		fric *= 0.5;
		fric += 0.5;
		return fric * fric*(3 - 2 * fric) * 2 - 1;
	}

	inline value_type ramp3over2(value_type x) {
		//return x < 4 ? 0.5*x*x : 4 * x - 8;
		return x * sqrt(x);
	}

	struct SimParams {
		value_type G;
		value_type C_TableRest;
		value_type C_TableStiff;
		value_type C_TableSlidFric;
		value_type C_TableRollFric;
		value_type C_TableSpinFric;
		value_type C_BallFric;
		value_type C_BallStiff;
		value_type C_BallRest;
		value_type Precision;
		value_type MaxStep;
	};

	class Simulation
	{
	public:
		Simulation(BallInfo* b_info, BallState* bs, size_t b_count, SimParams params);
		~Simulation();
		int Step(value_type dt);
		double CurrentTime();
		void RegisterActor(Actor* c);
		void Initialize();
		void operator()(const state_type &x, state_type &dxdt, const value_type t) const;
	private:
		static const size_t bsoffset = sizeof(BallState) / sizeof(value_type);
		stepper_type stepper;
		value_type t;
		state_type state;
		BallState* b_states;
		size_t b_count;

		std::vector<Ball> balls;
		std::vector<Actor*> actors;

		value_type G;
		value_type C_TableRest;
		value_type C_TableStiff;
		value_type C_TableSlidFric;
		value_type C_TableRollFric;
		value_type C_TableSpinFric;
		value_type C_BallFric;
		value_type C_BallStiff;
		value_type C_BallRest;
		value_type Precision;
		value_type MaxStep;

		inline void AddTableForce(const Ball &b, const BallState &bs, DBallState &db) const;
		inline void StepBall(const BallState &b, DBallState &db) const;
		inline void BallBallCollision(
			const Ball &b1,
			const Ball &b2,
			const BallState &bs1,
			const BallState &bs2,
			DBallState &db1,
			DBallState &db2) const;
	};
}

