#pragma once

namespace deadstroke {
	typedef double value_type;
	typedef glm::tvec3<value_type, glm::packed_highp> vec_type;
	typedef std::vector<value_type> state_type;
	typedef bulirsch_stoer_dense_out<state_type> stepper_type;
}