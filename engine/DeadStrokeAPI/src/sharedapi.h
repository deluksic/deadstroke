#pragma once
#include "stdafx.h"

#define SHARED_API __declspec(dllexport)

extern "C" {
	SHARED_API Simulation* CreateSimulation(BallInfo* b_info, BallState* bs, size_t b_count, SimParams params);
	SHARED_API void DeleteSimulation(Simulation* s);
	SHARED_API int StepSimulation(Simulation* s, value_type dt);
	SHARED_API void InitSimulation(Simulation* s);
	SHARED_API double GetTimeSimulation(Simulation* s);

	SHARED_API void RegisterPlaneCollider(Simulation* s, vec_type p, vec_type n, value_type c_rest, value_type c_fric, value_type c_stif);
	SHARED_API void RegisterCapsuleCollider(Simulation* s, vec_type p1, vec_type p2, value_type r, value_type c_rest, value_type c_fric, value_type c_stif);
	SHARED_API void RegisterBoundedPlaneCollider(Simulation* s, vec_type p, vec_type u, vec_type v, value_type c_rest, value_type c_fric, value_type c_stif);
	
	SHARED_API void RegisterRod(Simulation* s, int id, value_type d, vec_type p);
}

