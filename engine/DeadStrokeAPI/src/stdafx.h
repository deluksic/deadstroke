// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// Including SDKDDKVer.h defines the highest available Windows platform.

// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.

#include <SDKDDKVer.h>

#define _CRT_SECURE_NO_DEPRECATE		// Suppress unsafe warnings
#define _CRT_NONSTDC_NO_DEPRECATE		// Suppress unsafe warnings
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>

#include "deadstroke.h"
#include "type_definitions.h"
#include "ball.h"
#include "actor.h"
#include "collider.h"
#include "simulation.h"

using namespace deadstroke;
