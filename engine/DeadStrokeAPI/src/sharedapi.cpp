#include "stdafx.h"
#include "sharedapi.h"

SHARED_API Simulation* CreateSimulation(BallInfo* b_info, BallState* bs, size_t b_count, SimParams params) {
	return new Simulation(b_info, bs, b_count, params);
}

SHARED_API void DeleteSimulation(Simulation* s) {
	delete s;
}

SHARED_API int StepSimulation(Simulation* s, value_type dt) {
	return s->Step(dt);
}

SHARED_API void InitSimulation(Simulation* s) {
	s->Initialize();
}

SHARED_API double GetTimeSimulation(Simulation* s) {
	return s->CurrentTime();
}

SHARED_API void RegisterPlaneCollider(Simulation* s, vec_type p, vec_type n, value_type c_rest, value_type c_fric, value_type c_stif) {
	s->RegisterActor(new PlaneCollider(p, n, c_rest, c_fric, c_stif));
}

SHARED_API void RegisterCapsuleCollider(Simulation* s, vec_type p1, vec_type p2, value_type r, value_type c_rest, value_type c_fric, value_type c_stif) {
	s->RegisterActor(new CapsuleCollider(p1, p2, r, c_rest, c_fric, c_stif));
}

SHARED_API void RegisterBoundedPlaneCollider(Simulation* s, vec_type p, vec_type u, vec_type v, value_type c_rest, value_type c_fric, value_type c_stif) {
	s->RegisterActor(new BoundedPlaneCollider(p, u, v, c_rest, c_fric, c_stif));
}

SHARED_API void RegisterRod(Simulation* s, int id, value_type d, vec_type p) {
	s->RegisterActor(new Rod(id, d, p));
}