This folder should contain c++ libraries used by engine.

There is a zip file included with the libraries and should be extracted here to
get the following folder structure:

    ./
	    boost/		www.boost.org		(v1.64.0)
	    glm/		glm.g-truc.net		(v0.9.8)